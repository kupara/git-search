import React from 'react';
import { Box, Grommet } from 'grommet';
import { ApolloProvider } from '@apollo/client';

import Main from './main';
import { MainProvider } from './main/context';
import { theme } from './theme';
import { client } from './apollo/client';

function App() {
  return (
    <Grommet theme={theme}>
      <ApolloProvider client={client}>
        <MainProvider>
          <Box align='center' width='80rem' margin='auto'>
            <Main />
          </Box>
        </MainProvider>
      </ApolloProvider>
    </Grommet>
  );
}

export default App;

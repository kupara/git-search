import { gql } from '@apollo/client';

export const searchUsersQuery = gql`
  query SearchUsers($search: String!) {
    search(query: $search, type: USER, first: 20) {
      edges {
        node {
          ... on User {
            avatarUrl
            id
            login
            name
            repositories {
              totalCount
            }
            starredRepositories {
              totalCount
            }
          }
        }
      }
    }
  }
`;

export const userRepositoriesQuery = gql`
  query Repos($login: String!, $cursor: String) {
    user(login: $login) {
      login
      name
      repositories(first: 50, after: $cursor) {
        edges {
          cursor
          node {
            name
            watchers {
              totalCount
            }
            stargazerCount
          }
        }
      }
    }
  }
`;

export const repositoryQuery = gql`
  query Repo($login: String!, $name: String!, $cursor: String) {
    repository(owner: $login, name: $name) {
      id
      name
      stargazerCount
      watchers {
        totalCount
      }
      issues(first: 50, states: OPEN, after: $cursor) {
        edges {
          node {
            body
            number
            title
            author {
              login
            }
            createdAt
          }
        }
      }
    }
  }
`;

export const createIssueMutation = gql`
  mutation CreateIssue($input: CreateIssueInput!) {
    createIssue(input: $input) {
      clientMutationId
      issue {
        id
      }
    }
  }
`;

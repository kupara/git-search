export type SearchResultNode = {
  __typename?: 'User' | 'Organization';
  avatarUrl: string;
  id: string;
  login: string;
  name: string;
  repositories: {
    __typename: 'RepositoryConnection';
    totalCount: number;
  };
  starredRepositories: {
    __typename: 'StarredRepositoryConnection';
    totalCount: number;
  };
};

export type UserSearchResultEdge = {
  __typename: 'SearchResultItemEdge';
  node: SearchResultNode;
};

export type UserSearchResult = {
  __typename?: 'SearchResultItemConnection';
  search: {
    __typename: 'SearchResultItemConnection';
    edges: UserSearchResultEdge[];
  };
};

export type UserSearchVariables = {
  search: string;
};

export type UserQueryVariables = {
  login: string;
  cursor?: string;
};

export type IssueEdge = {
  __typename: 'IssueEdge';
  cursor: string;
  node: {
    __typename: 'Issue';
    author: UserResult;
    body: string;
    createdAt: string;
    number: number;
    title: string;
  };
};

type Repository = {
  __typename: 'Repository';
  id: string;
  name: string;
  watchers: {
    __typename: 'UserConnection';
    totalCount: number;
  };
  stargazerCount: number;
};

export type RepoEdge = {
  __typename: 'RepositoryEdge';
  cursor: string;
  node: Repository;
  issues: {
    __typename: 'IssueConnection';
    edges: IssueEdge[];
  };
};

export type UserResult = {
  __typename?: 'User';
  login: string;
  name: string;
  repositories: {
    __typename: 'RepositoryConnection';
    edges: RepoEdge[];
  };
};

export type UserQueryResult = {
  user: UserResult;
};

export type RepoQueryVariables = {
  login: string;
  name: string;
  cursor?: string;
};

export type RepoQueryResult = {
  repository: Repository & {
    issues: {
      __typename: 'IssueConnection';
      edges: IssueEdge[];
    };
  };
};

export type CreateIssueVariables = {
  input: {
    repositoryId: string;
    title: string;
    body: string;
  };
};

export type CreateIssueResult = {
  createIssue: {
    clientMutationId: string;
    issue: {
      id: string;
    };
  };
};

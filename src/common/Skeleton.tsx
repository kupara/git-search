import React from 'react';
import styled, { keyframes, css } from 'styled-components';

interface StyleProps {
  borderRadius?: string;
  height?: string | number;
  inline?: boolean;
  margin?: string;
  width?: string | number;
}

const skeletonAnimation = keyframes`
  0% {
    opacity: 1;
  }
  50% {
    opacity: 0.5;
  }
  100% {
    opacity: 1;
  }
`;

const animationStyles = css`
  animation-name: ${skeletonAnimation};
  animation-duration: 1.5s;
  animation-timing-function: ease-in-out;
  animation-iteration-count: infinite;
`;

const SkeletonBox = styled.span`
  position: initial;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  display: block;
  width: ${({ width }: StyleProps): number | string => width || '100%'};
  height: ${({ height }): number | string => height || '100%'};
  margin: ${({ margin }: StyleProps): string => margin || '0'};
  background: ${({
    theme: {
      global: { colors },
    },
  }): string => colors.gray};
  border-radius: ${({ borderRadius }: StyleProps): string =>
    borderRadius || '1.875rem'};
  ${animationStyles};
`;

interface SkeletonProps {
  borderRadius?: string;
  children: JSX.Element;
  height: string | number;
  loading: boolean;
  margin?: string;
  width: string | number;
}

const Skeleton = ({
  children,
  loading,
  width = '100%',
  height = 'auto',
  margin,
  borderRadius,
}: SkeletonProps): JSX.Element => {
  return loading ? (
    <SkeletonBox
      width={width}
      height={height}
      borderRadius={borderRadius}
      margin={margin}
    />
  ) : (
    children
  );
};

Skeleton.defaultProps = {
  loading: false,
  height: '100%',
  width: '100%',
  className: undefined,
  children: null,
};

export default Skeleton;

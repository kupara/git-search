import React from 'react';
import { Box, Text } from 'grommet';

type Props = {
  message: string;
};

export const Status = ({ message }: Props) => (
  <Box align='center' justify='center' height='10rem'>
    <Text color='dark'>{message}</Text>;
  </Box>
);

export default Status;

import React from 'react';
import { Box } from 'grommet';

import SearchBox from '../search';
import UserList from '../users/List';

import RepoList from '../repos';
import Issues from '../repo';
import { useMainContext } from './context';

const Main = (): JSX.Element => {
  const { selectedRepo, selectedUser } = useMainContext();

  const showRepo = Boolean(selectedRepo && selectedUser);
  return (
    <Box align='center' pad='2rem' fill>
      <SearchBox />
      {showRepo ? (
        <Issues />
      ) : (
        <>
          <UserList />
          <RepoList />
        </>
      )}
    </Box>
  );
};

export default Main;

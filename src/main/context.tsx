import React, { createContext, useContext, useState, memo } from 'react';

interface Context {
  search: string;
  selectedRepo: string;
  selectedUser: string;
  setSearch: React.Dispatch<React.SetStateAction<string>>;
  setSelectedRepo: React.Dispatch<React.SetStateAction<string>>;
  setSelectedUser: React.Dispatch<React.SetStateAction<string>>;
}

interface Props {
  children: React.ReactNode;
}

export const MainContext = createContext<Context>({
  search: '',
  selectedRepo: '',
  selectedUser: '',
  setSearch: () => {},
  setSelectedRepo: () => {},
  setSelectedUser: () => {},
});

const Provider = ({ children }: Props) => {
  const [search, setSearch] = useState('');
  const [selectedRepo, setSelectedRepo] = useState('');
  const [selectedUser, setSelectedUser] = useState('');

  return (
    <MainContext.Provider
      value={{
        search,
        selectedRepo,
        selectedUser,
        setSearch,
        setSelectedRepo,
        setSelectedUser,
      }}
    >
      {children}
    </MainContext.Provider>
  );
};

export const MainProvider = memo(Provider);

export const useMainContext = () => {
  const context = useContext(MainContext);
  if (!context) {
    throw new Error('useMainContext must be used within a MainProvider');
  }
  return context;
};

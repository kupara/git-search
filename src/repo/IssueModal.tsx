import React, { useState } from 'react';
import { ApolloError, useMutation } from '@apollo/client';
import {
  Box,
  Button,
  FormField,
  Layer,
  Text,
  TextInput,
  TextArea,
  Heading,
} from 'grommet';

import { createIssueMutation } from '../apollo/queries';
import { CreateIssueVariables, CreateIssueResult } from '../apollo/types';

interface Props {
  closeModal: () => void;
  open: boolean;
  repositoryId: string;
}

const IssueModal = ({ open, closeModal, repositoryId }: Props) => {
  const [state, setState] = useState({
    title: '',
    description: '',
  });

  const [error, setError] = useState<string | null>(null);

  const [createIssue, { loading }] = useMutation<
    CreateIssueResult,
    CreateIssueVariables
  >(createIssueMutation, {
    onCompleted: closeModal,
    onError: (error: ApolloError) => {
      setError(error.message ?? 'Something went wrong. Try again later');
    },
    refetchQueries: ['Repo'],
  });

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = e.target;

    setState((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleCreate = () => {
    createIssue({
      variables: {
        input: {
          title: state.title,
          body: state.description,
          repositoryId,
        },
      },
    });
  };

  const saveDisabled = !state.title || !state.description || loading;

  return open ? (
    <Layer onEsc={closeModal}>
      <Box pad='2rem' width='56rem'>
        <Heading color='dark' level={3}>
          Create a new issue
        </Heading>
        <FormField label='Title'>
          <TextInput name='title' onChange={handleChange} plain={false} />
        </FormField>
        <FormField label='Description'>
          <TextArea name='description' onChange={handleChange} plain={false} />
        </FormField>
        {error && <Text color='red'>{error}</Text>}
        <Box direction='row' justify='end' gap='1rem'>
          <Button label='Cancel' onClick={closeModal} />
          <Button
            label='Submit'
            onClick={handleCreate}
            primary
            disabled={saveDisabled}
          />
        </Box>
      </Box>
    </Layer>
  ) : null;
};

export default IssueModal;

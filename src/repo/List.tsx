import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import { Box, Button, Heading, List, Text } from 'grommet';
import styled from 'styled-components';

import { repositoryQuery } from '../apollo/queries';

import {
  IssueEdge,
  RepoQueryVariables,
  RepoQueryResult,
} from '../apollo/types';
import { Skeleton, Status } from '../common';
import { useMainContext } from '../main/context';
import IssueModal from './IssueModal';

const StyledBox = styled(Box)`
  > ul {
    width: 100%;
  }

  & li:hover {
    background-color: ${({ theme }) => theme.global.colors.light};
  }
`;

const formatDate = (date: string): string => {
  const dateObj = new Date(date);
  return dateObj.toDateString();
};

const IssuesList = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const { selectedUser, selectedRepo } = useMainContext();
  const { data, loading, error } = useQuery<
    RepoQueryResult,
    RepoQueryVariables
  >(repositoryQuery, {
    variables: {
      login: selectedUser,
      name: selectedRepo,
    },
    skip: !selectedUser && !selectedRepo,
  });

  const repo = data?.repository;
  const issues = data?.repository.issues.edges ?? [];

  if (error) {
    return <Status message='We could not find or fetch this repo .' />;
  }

  const showNoIssuesMessage = issues?.length === 0 && !loading;

  return (
    <StyledBox align='start' fill margin={{ top: 'medium' }} width='100%'>
      <Box justify='between' direction='row' width='100%'>
        <Heading alignSelf='center' level={2} margin='0'>
          {repo?.name}
        </Heading>
        <Box direction='row' gap='small' align='center'>
          <Text color='gray'>{repo?.stargazerCount} stars •</Text>
          <Text color='gray'>{repo?.watchers.totalCount} watchers</Text>
        </Box>
      </Box>
      <Box
        direction='row'
        justify='between'
        margin={{ top: '1rem' }}
        width='100%'
      >
        <Heading level={4} margin={{ vertical: '0.5rem', horizontal: '0' }}>
          Open issues
        </Heading>
        <Button
          label='Create Issue'
          onClick={(): void => setModalOpen(true)}
          primary
          size='small'
        />
      </Box>
      <Skeleton loading={loading} height='10rem' width='100%'>
        {showNoIssuesMessage ? (
          <Status message='This repo has no issues, you can open one using the create button above' />
        ) : (
          <List
            data={issues}
            children={(item: IssueEdge) => (
              <Box justify='between' flex>
                <Text>{item.node.title}</Text>
                <Box gap='small' direction='row'>
                  <Text color='gray'>#{item.node.number}</Text>
                  <Text color='gray'>
                    Opened on {formatDate(item.node.createdAt)}
                  </Text>
                  <Text color='gray'>by {item.node.author.login}</Text>
                </Box>
              </Box>
            )}
            margin={{ vertical: '1rem' }}
          />
        )}
      </Skeleton>
      {
        <IssueModal
          open={modalOpen}
          closeModal={() => setModalOpen(false)}
          repositoryId={repo?.id ?? ''}
        />
      }
    </StyledBox>
  );
};

export default IssuesList;

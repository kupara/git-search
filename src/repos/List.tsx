import React from 'react';
import { useQuery } from '@apollo/client';
import { Box, Heading, List, Text } from 'grommet';
import styled from 'styled-components';

import { userRepositoriesQuery } from '../apollo/queries';

import { RepoEdge, UserQueryResult, UserQueryVariables } from '../apollo/types';
import { Skeleton, Status } from '../common';
import { useMainContext } from '../main/context';

const StyledBox = styled(Box)`
  > div {
    width: 100%;
  }

  & li:hover {
    background-color: ${({ theme }) => theme.global.colors.light};
  }
`;

const RepoList = () => {
  const { selectedUser, setSelectedRepo } = useMainContext();
  const { data, loading, error } = useQuery<
    UserQueryResult,
    UserQueryVariables
  >(userRepositoriesQuery, {
    variables: {
      login: selectedUser,
    },
    skip: !selectedUser,
  });

  const repos = data?.user?.repositories.edges;

  if (error) {
    return <Status message='Something went wrong. Please try again later.' />;
  }

  if (repos?.length === 0 && !loading) {
    return <Status message='Select a user above to fetch their repos' />;
  }

  return selectedUser ? (
    <StyledBox align='start' fill margin={{ top: 'medium' }}>
      <Heading level={5}>User repositories</Heading>
      <Skeleton loading={loading} height='10rem' width='100%'>
        <List
          data={repos}
          children={(item: RepoEdge) => (
            <Box direction='row' justify='between' flex>
              <Text>{item.node.name}</Text>
              <Box direction='row' gap='small'>
                <Text color='gray'>{item.node.stargazerCount} stars •</Text>
                <Text color='gray'>
                  {item.node.watchers.totalCount} watchers
                </Text>
              </Box>
            </Box>
          )}
          onClickItem={(event: any) => {
            setSelectedRepo(event.item.node.name);
          }}
          paginate
          step={10}
          show={{ page: 1 }}
        />
      </Skeleton>
    </StyledBox>
  ) : null;
};

export default RepoList;

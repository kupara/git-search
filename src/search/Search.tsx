import React from 'react';
import { Box, TextInput } from 'grommet';
import { Search } from 'grommet-icons';

import { useMainContext } from '../main/context';

const SearchBox = () => {
  const { search, setSearch, setSelectedRepo, setSelectedUser } =
    useMainContext();

  const handleSearch = (val: string): void => {
    setSearch(val);
    setSelectedRepo('');
    setSelectedUser('');
  };
  return (
    <Box width='50%' justify='center'>
      <TextInput
        icon={<Search />}
        name='search'
        onChange={(e) => handleSearch(e.target.value)}
        placeholder='Search users...'
        reverse
        value={search}
      />
    </Box>
  );
};

export default SearchBox;

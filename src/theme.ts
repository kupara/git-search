import { ThemeType } from 'grommet';

export const theme: ThemeType = {
  global: {
    font: {
      family: 'Roboto, sans-serif',
    },
    colors: {
      accent: '#17D7A0',
      active: '#6166B3',
      brand: '#4D77FF',
      dark: '#000000',
      focus: '#6166B3',
      gray: '#828282',
      light: '#F8F8F8',
      pink: '#F05454',
      selected: '#6166B3',
      text: '#4e4e4e',
    },
    input: {
      font: {
        weight: 'normal',
      },
    },
  },
  button: {
    color: '#000000',
  },
  formField: {
    border: {
      color: 'transparent',
    },
    label: {
      margin: {
        left: '0',
      },
      color: 'gray',
    },
  },
};

import React, { useState } from 'react';
import { Box, Image, Text } from 'grommet';

import { SearchResultNode } from '../apollo/types';
import { useMainContext } from '../main/context';

interface Props {
  data: SearchResultNode;
  loading: boolean;
}

const Item = ({
  data: { avatarUrl, login, name, repositories, starredRepositories },
}: Props) => {
  const [hovered, setHovered] = useState(false);
  const { selectedUser, setSelectedUser } = useMainContext();
  const isSelected = selectedUser === login;
  return (
    <Box
      align='center'
      background={isSelected ? 'grey' : 'dark'}
      height='10rem'
      justify='center'
      onClick={() => {
        setSelectedUser(login);
      }}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      round='xsmall'
      width={{ min: '16rem' }}
    >
      {hovered ? (
        <Box>
          <Image fit='cover' src={avatarUrl} />
        </Box>
      ) : (
        <>
          <Text color='white' size='medium'>
            {name}
          </Text>
          <Box direction='row' justify='between' pad='small' gap='medium'>
            <Text color='white' size='medium'>
              {repositories?.totalCount ?? 0} Repos
            </Text>
            <Text color='white' size='medium'>
              {starredRepositories?.totalCount ?? 0} Stars
            </Text>
          </Box>
        </>
      )}
    </Box>
  );
};

export default Item;

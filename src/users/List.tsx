import React from 'react';
import { useQuery } from '@apollo/client';
import { Heading, Box } from 'grommet';

import { searchUsersQuery } from '../apollo/queries';

import Item from './Item';
import {
  UserSearchResultEdge,
  UserSearchResult,
  UserSearchVariables,
} from '../apollo/types';
import { Skeleton, Status } from '../common';
import { useMainContext } from '../main/context';
import filterUsers from './filterUsers';

const List = () => {
  const { search } = useMainContext();
  const { data, loading, error } = useQuery<
    UserSearchResult,
    UserSearchVariables
  >(searchUsersQuery, {
    variables: {
      search,
    },
  });

  const users = data?.search?.edges ?? [];

  if (error) {
    return <Status message='Something went wrong. Please try again later.' />;
  }

  if (users?.length === 0 && !loading) {
    return <Status message='No users found for this search string' />;
  }

  const filteredUsers = filterUsers(users as UserSearchResultEdge[]);

  return (
    <Box align='start' fill>
      <Heading
        alignSelf='center'
        color='gray'
        level={3}
        margin='medium'
        textAlign='center'
      >
        Results
      </Heading>
      <Heading
        alignSelf='start'
        color='dark'
        level={4}
        margin='small'
        textAlign='center'
      >
        Users
      </Heading>
      <Skeleton loading={loading} height='10rem' width='100%'>
        <Box direction='row' gap='medium' overflow='auto'>
          {filteredUsers?.map(({ node }) => (
            <Item key={node.id} data={node} loading={loading} />
          ))}
        </Box>
      </Skeleton>
    </Box>
  );
};

export default List;

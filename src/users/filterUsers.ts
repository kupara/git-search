import { UserSearchResultEdge } from '../apollo/types';

const filterUsers = (results: UserSearchResultEdge[]) =>
  results.filter(({ node }) => node.__typename === 'User');

export default filterUsers;
